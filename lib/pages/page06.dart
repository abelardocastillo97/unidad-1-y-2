import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() => runApp(page06());

class page06 extends StatelessWidget {
  const page06({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Mi App",
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  static AudioCache player = AudioCache(prefix: 'assets/');
  AudioPlayer audioPlayer = AudioPlayer();
  late AudioCache audioCache;

  playMusic(String filePath) async {
    audioPlayer = AudioPlayer();
    audioCache = AudioCache(fixedPlayer: audioPlayer);
    await audioCache.play(filePath);
  }

  stopMusic() async {
    await audioPlayer.stop();
  }

  String song = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/Background.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Center(
                child: ListView(
              children: [
                Image.asset("assets/MOTS.jpg"),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '01.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '01.mp3') {
                        stopMusic();
                        song = '01.mp3';
                        playMusic(song);
                      } else if (song == '01.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          🪐",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '02.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '02.mp3') {
                        stopMusic();
                        song = '02.mp3';
                        playMusic(song);
                      } else if (song == '02.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          Higher Power",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '03.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '03.mp3') {
                        stopMusic();
                        song = '03.mp3';
                        playMusic(song);
                      } else if (song == '03.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          Humankind",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '04.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '04.mp3') {
                        stopMusic();
                        song = '04.mp3';
                        playMusic(song);
                      } else if (song == '04.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          ✨",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '05.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '05.mp3') {
                        stopMusic();
                        song = '05.mp3';
                        playMusic(song);
                      } else if (song == '05.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          Let Somebody Go",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '06.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '06.mp3') {
                        stopMusic();
                        song = '06.mp3';
                        playMusic(song);
                      } else if (song == '06.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          ❤️",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '07.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '07.mp3') {
                        stopMusic();
                        song = '07.mp3';
                        playMusic(song);
                      } else if (song == '07.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          People of The Pride",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '08.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '08.mp3') {
                        stopMusic();
                        song = '08.mp3';
                        playMusic(song);
                      } else if (song == '08.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          Biutyful",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '09.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '09.mp3') {
                        stopMusic();
                        song = '09.mp3';
                        playMusic(song);
                      } else if (song == '09.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          🌎",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '10.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '10.mp3') {
                        stopMusic();
                        song = '10.mp3';
                        playMusic(song);
                      } else if (song == '10.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          My Universe",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '11.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '11.mp3') {
                        stopMusic();
                        song = '11.mp3';
                        playMusic(song);
                      } else if (song == '11.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          ♾️",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
                OutlinedButton(
                    onPressed: () {
                      if (audioPlayer.state == PlayerState.STOPPED) {
                        song = '12.mp3';
                        playMusic(song);
                      } else if (audioPlayer.state != PlayerState.STOPPED &&
                          song != '12.mp3') {
                        stopMusic();
                        song = '12.mp3';
                        playMusic(song);
                      } else if (song == '12.mp3') {
                        stopMusic();
                      }
                    },
                    child: Row(
                      children: [
                        Icon(Icons.play_circle, color: Colors.white),
                        Text("          Coloratura",
                            style: TextStyle(color: Colors.white)),
                      ],
                    )),
              ],
            ))));
  }
}
