import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() => runApp(page07());

class page07 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Google Maps Demo',
      home: MapSample(),
    );
  }
}

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kITD = CameraPosition(
    target: LatLng(28.2155117, -105.4349936),
    zoom: 17,
  );

  void obtenerUbicacion() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    String posicion = position.toString();

    final startIndex = posicion.indexOf("Latitude:");
    final endIndex = posicion.indexOf("Longitude");

    double latitud =
        double.parse(posicion.substring(startIndex + 10, endIndex - 2));
    double longitud = double.parse(posicion.substring(endIndex + 11));

    CameraPosition _actual = CameraPosition(
        target: LatLng(latitud, longitud), zoom: 19.151926040649414);

    _goToCurrent(_actual);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.hybrid,
        initialCameraPosition: _kITD,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          obtenerUbicacion();
        },
        label: Text('Ubicacion Actual'),
        icon: Icon(Icons.location_on),
      ),
    );
  }

  Future<void> _goToCurrent(CameraPosition _ubicacion) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_ubicacion));
  }
}
