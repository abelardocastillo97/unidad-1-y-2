import 'package:flutter/material.dart';
import 'dart:async';
import 'package:compasstools/compasstools.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(page09());
  });
}

class page09 extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<page09> {
  late int _haveSensor;
  late String sensorType;

  @override
  void initState() {
    super.initState();
    checkDeviceSensors();
  }

  Future<void> checkDeviceSensors() async {
    late int haveSensor;

    try {
      haveSensor = await Compasstools.checkSensors;

      switch (haveSensor) {
        case 0:
          {
            // statements;
            sensorType = "No se encontraron sensores";
          }
          break;

        case 1:
          {
            //statements;
            sensorType = "Acelerometro + Magnetometro";
          }
          break;

        case 2:
          {
            //statements;
            sensorType = "Giroscopio";
          }
          break;

        default:
          {
            //statements;
            sensorType = "Error!";
          }
          break;
      }
    } on Exception {}

    if (!mounted) return;

    setState(() {
      _haveSensor = haveSensor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                StreamBuilder(
                  stream: Compasstools.azimuthStream,
                  builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
                    if (snapshot.hasData) {
                      return Padding(
                        padding: EdgeInsets.all(20),
                        child: Center(
                          child: new RotationTransition(
                            turns: new AlwaysStoppedAnimation(
                                -snapshot.data! / 360),
                            child: Image.asset("assets/compass.png"),
                          ),
                        ),
                      );
                    } else
                      return Text("Error");
                  },
                ),
                Text("Tipo de Sensor: " + sensorType),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
