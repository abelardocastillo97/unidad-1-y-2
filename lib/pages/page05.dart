import 'package:flutter/material.dart';
import 'package:unidad_1_y_2/pages/page01.dart';
import 'package:unidad_1_y_2/pages/page02.dart';
import 'package:unidad_1_y_2/pages/page03.dart';
import 'package:unidad_1_y_2/pages/page04.dart';
import 'package:unidad_1_y_2/pages/page05.dart';
import 'package:unidad_1_y_2/pages/page06.dart';
import 'package:unidad_1_y_2/pages/page07.dart';
import 'package:unidad_1_y_2/pages/page08.dart';
import 'package:unidad_1_y_2/pages/page09.dart';

void main() => runApp(const page05());

class page05 extends StatelessWidget {
  const page05({Key? key}) : super(key: key);

  static const appTitle = '1.5 Action Bar y Menús';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.blueGrey,
      ),
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
          ),
          padding: EdgeInsets.all(15),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.chevron_left, size: 100, color: Colors.white),
              Text(
                  "Presiona o arrastra el menú lateral para acceder a los temas de la unidad 1 & 2",
                  style: TextStyle(color: Colors.white, fontSize: 25)),
              Icon(Icons.chevron_left, size: 100, color: Colors.white)
            ],
          ))),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blueGrey,
                ),
                child: Icon(Icons.album, size: 100, color: Colors.white)),
            ListTile(
              title: const Text(
                  '1.1 Layouts, botones, etiquetas y cuadros de texto'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page01()));
              },
            ),
            ListTile(
              title: const Text('1.2 Checkboxes y Radio Buttons'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page02()));
              },
            ),
            ListTile(
              title: const Text('1.3 Listas simples y desplegables'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page03()));
              },
            ),
            ListTile(
              title: const Text('1.4 Grids y Pestañas'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page04()));
              },
            ),
            ListTile(
              title: const Text('1.5 Action Bar y Menús'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page05()));
              },
            ),
            ListTile(
              title: const Text('1.6 Imágenes y elementos multimedia'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page06()));
              },
            ),
            ListTile(
              title: const Text('2.2 Geolocalización'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page07()));
              },
            ),
            ListTile(
              title: const Text('2.3 Orientación de los dispositivos'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page08()));
              },
            ),
            ListTile(
              title: const Text('2.4 Movimiento en los dispositivos'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => page09()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
