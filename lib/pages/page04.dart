import 'package:flutter/material.dart';

void main() {
  runApp(page04());
}

class page04 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(text: "Inicio"),
                Tab(text: "Discografía"),
                Tab(icon: Icon(Icons.shopping_bag)),
              ],
            ),
            title: Text('1.4 Grids y Pestañas'),
            backgroundColor: Colors.black,
          ),
          body: TabBarView(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/Background.jpg"),
                      fit: BoxFit.cover),
                ),
                child: Center(
                    child: ListView(
                  children: [
                    Text("Radiohead",
                        style: TextStyle(
                            height: 2, color: Colors.white, fontSize: 30)),
                    SizedBox(height: 50),
                    SizedBox(
                        width: 150,
                        height: 200,
                        child: Image.asset("assets/radio.png")),
                    SizedBox(height: 50),
                    Text(
                        "Radiohead es una banda británica de rock alternativo originaria de Abingdon-on-Thames, Inglaterra, formada en 1985 inicialmente como una banda de versiones. Está integrada por Thom Yorke (voz, guitarra, piano), Jonny Greenwood (guitarra solista, teclados, otros instrumentos), Ed O'Brien (guitarra, segunda voz), Colin Greenwood (bajo, teclados) y Phil Selway (batería, percusión).",
                        style: TextStyle(color: Colors.white, fontSize: 15)),
                    SizedBox(height: 50),
                    SizedBox(
                        width: 150,
                        height: 200,
                        child: Image.asset("assets/radiohead.jpg")),
                    SizedBox(height: 50),
                    Text(
                        "Radiohead lanzó su primer sencillo, «Creep», en 1992. Si bien la canción fue en un comienzo un fracaso comercial, se convirtió en un éxito mundial tras el lanzamiento de su álbum debut, Pablo Honey (1993) debido al boom comercial del rock alternativo. La popularidad de Radiohead en el Reino Unido aumentó con su segundo álbum, The Bends (1995). El tercero, OK Computer (1997), con un sonido expansivo y temáticas como la alienación y la globalización, les dio fama mundial y ha sido aclamado como un disco histórico de la década de 1990 y uno de los mejores álbumes de todos los tiempos.",
                        style: TextStyle(color: Colors.white, fontSize: 15)),
                    SizedBox(height: 50),
                    SizedBox(
                        width: 150,
                        height: 200,
                        child: Image.asset("assets/radiologo.jpg")),
                    SizedBox(height: 50),
                    Text(
                        "Kid A (2000) y Amnesiac (2001) significaron una evolución en su estilo musical, al incorporar música electrónica experimental, música clásica del siglo XX, Trip-Hop y Cool Jazz. A pesar de la división inicial de fanes y crítica, Kid A fue nombrado mejor álbum de la década por Rolling Stone, Pitchfork y The Times. El álbum Hail to the Thief (2003), una mezcla de rock y música electrónica con letras inspiradas en la guerra al terror, fue el último de la banda con el sello discográfico EMI.​ Radiohead lanzó de manera independiente su séptimo álbum, In Rainbows (2007), que fue puesto a la venta en forma de descarga digital por la que los usuarios pagaban el precio que estimasen oportuno. Su octavo álbum, The King of Limbs (2011), fue una exploración del ritmo y de texturas más calmadas. En su noveno álbum, A Moon Shaped Pool (2016), predominan los arreglos orquestales de Jonny Greenwood.​",
                        style: TextStyle(color: Colors.white, fontSize: 15)),
                    SizedBox(height: 50),
                  ],
                )),
              ),
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/Background.jpg"),
                      fit: BoxFit.cover),
                ),
                child: Center(
                  child: GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    children: [
                      Image.asset('assets/pablo.jpg'),
                      Image.asset('assets/bends.jpg'),
                      Image.asset('assets/ok.jpg'),
                      Image.asset('assets/kid.jpg'),
                      Image.asset('assets/amnesiac.jpg'),
                      Image.asset('assets/hail.jpg'),
                      Image.asset('assets/rain.jpg'),
                      Image.asset('assets/king.jpg'),
                      Image.asset('assets/moon.jpg')
                    ],
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/Background.jpg"),
                      fit: BoxFit.cover),
                ),
                child: Center(
                  child: GridView(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    children: [
                      Image.asset('assets/s1.webp'),
                      Image.asset('assets/s2.webp'),
                      Image.asset('assets/s3.webp'),
                      Image.asset('assets/s4.webp'),
                      Image.asset('assets/s5.webp'),
                      Image.asset('assets/s6.webp'),
                      Image.asset('assets/s7.webp'),
                      Image.asset('assets/s8.webp'),
                      Image.asset('assets/s9.webp'),
                      Image.asset('assets/s10.webp')
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
