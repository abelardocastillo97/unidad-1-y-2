import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() => runApp(page08());

class page08 extends StatelessWidget {
  const page08({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Mi App",
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  String _orientacion = 'Portrait Up';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/Background.jpg"),
                  fit: BoxFit.cover),
            ),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("2.3 Orientación de los dispositivos",
                    style: TextStyle(color: Colors.white, fontSize: 20)),
                SizedBox(height: 40),
                RaisedButton(
                    child: Text("Landscape Left"),
                    onPressed: () {
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.landscapeLeft]);
                    }),
                RaisedButton(
                    child: Text("Portrait Up"),
                    onPressed: () {
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.portraitUp]);
                    }),
                RaisedButton(
                    child: Text("Landscape Right"),
                    onPressed: () {
                      SystemChrome.setPreferredOrientations(
                          [DeviceOrientation.landscapeRight]);
                    })
              ],
            ))));
  }
}
