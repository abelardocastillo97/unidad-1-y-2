import 'package:flutter/material.dart';

void main() => runApp(page03());

class page03 extends StatelessWidget {
  const page03({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Mi App",
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  var _lista = ['Super Mario Bros', 'The Legend of Zelda', 'Metroid', 'Kirby'];

  String _vista = 'Esta es una lista desplegable';

  String img1 = 'assets/img.png';
  String img2 = 'assets/img.png';
  String img3 = 'assets/img.png';
  String img4 = 'assets/img.png';
  String img5 = 'assets/img.png';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
          ),
          child: Center(
            child: ListView(
              padding: EdgeInsets.all(30),
              children: [
                SizedBox(
                  height: 50,
                ),
                Text('1.3 Listas simples y desplegables',
                    style: TextStyle(fontSize: 24, color: Colors.white)),
                SizedBox(
                  height: 50,
                ),
                DropdownButton(
                    items: _lista.map((String a) {
                      return DropdownMenuItem(value: a, child: Text(a));
                    }).toList(),
                    onChanged: (_value) => {
                          setState(() {
                            _vista = _value.toString();
                          }),
                          if (_vista == 'Super Mario Bros')
                            {
                              img1 = 'assets/smb1.jpg',
                              img2 = 'assets/smb2.png',
                              img3 = 'assets/smb3.jpg',
                              img4 = 'assets/smw.jpg',
                              img5 = 'assets/sm64.jpg'
                            }
                          else if (_vista == 'The Legend of Zelda')
                            {
                              img1 = 'assets/z1.jpg',
                              img2 = 'assets/z2.jpg',
                              img3 = 'assets/z3.jpg',
                              img4 = 'assets/z4.jpg',
                              img5 = 'assets/z5.jpg'
                            }
                          else if (_vista == 'Metroid')
                            {
                              img1 = 'assets/m1.jpg',
                              img2 = 'assets/m2.jpg',
                              img3 = 'assets/m3.jpg',
                              img4 = 'assets/m4.jpg',
                              img5 = 'assets/m5.jpg'
                            }
                          else if (_vista == 'Kirby')
                            {
                              img1 = 'assets/k1.jpg',
                              img2 = 'assets/k2.jpg',
                              img3 = 'assets/k3.jpg',
                              img4 = 'assets/k4.jpg',
                              img5 = 'assets/k5.jpg'
                            }
                        },
                    hint: Text(_vista,
                        style: TextStyle(
                            color: Color.fromARGB(255, 224, 224, 224)))),
                imagen1(img1),
                imagen2(img2),
                imagen3(img3),
                imagen4(img4),
                imagen5(img5),
              ],
            ),
          )),
    );
  }
}

Widget imagen1(String img1) {
  return SizedBox(width: 250, height: 300, child: Image.asset(img1));
}

Widget imagen2(String img2) {
  return SizedBox(width: 250, height: 300, child: Image.asset(img2));
}

Widget imagen3(String img3) {
  return SizedBox(width: 250, height: 300, child: Image.asset(img3));
}

Widget imagen4(String img4) {
  return SizedBox(width: 250, height: 300, child: Image.asset(img4));
}

Widget imagen5(String img5) {
  return SizedBox(width: 250, height: 300, child: Image.asset(img5));
}
