import 'package:flutter/material.dart';

void main() => runApp(page01());

class page01 extends StatelessWidget {
  const page01({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Mi App",
      home: Inicio(),
    );
  }
}

class Inicio extends StatefulWidget {
  Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

final myController = TextEditingController();
final myController2 = TextEditingController();

class _InicioState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: cuerpo(),
    );
  }
}

Widget cuerpo() {
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
          image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
    ),
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: EdgeInsets.all(15), //apply padding to all four sides
            child: Text("1.1 Layouts, botones, etiquetas y cuadros de texto",
                style: TextStyle(
                    color: Color.fromARGB(255, 224, 224, 224), fontSize: 20)),
          ),
          campoTexto(),
          SizedBox(
            height: 50,
          ),
          boton(),
          SizedBox(
            height: 50,
          ),
          resultado(""),
        ],
      ),
    ),
  );
}

Widget campoTexto() {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
    child: TextField(
      controller: myController,
      decoration: InputDecoration(
        hintText: "Ingresa un Texto",
        fillColor: Color.fromARGB(255, 255, 255, 255),
        filled: true,
      ),
    ),
  );
}

Widget boton() {
  return FlatButton(
    color: Colors.blueGrey,
    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
    onPressed: () {
      invertirPalabra();
    },
    child: Text("Invertir Palabra",
        style: TextStyle(fontSize: 25, color: Colors.white)),
  );
}

Widget resultado(String _palabra) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
    child: TextField(
      controller: myController2,
      enabled: false,
      decoration: InputDecoration(
        hintText: _palabra,
        fillColor: Color.fromARGB(255, 255, 255, 255),
        filled: true,
      ),
    ),
  );
}

void invertirPalabra() {
  String _palabra = myController.text;
  String _palabraInv = '';
  for (var i = _palabra.length - 1; i >= 0; i--) {
    _palabraInv += String.fromCharCode(_palabra.runes.elementAt(i));
  }
  myController2.text = _palabraInv;
}
