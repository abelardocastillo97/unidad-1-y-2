import 'package:flutter/material.dart';

void main() {
  runApp(page02());
}

class page02 extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Color _bulbColor = Color.fromARGB(255, 0, 0, 0);
  bool _isActive = false;

  String colorGroupValue = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
      ),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Padding(
            padding: EdgeInsets.all(15), //apply padding to all four sides
            child: Text("1.2 Checkboxes y Radio Buttons",
                style: TextStyle(
                    color: Color.fromARGB(255, 224, 224, 224), fontSize: 20)),
          ),
          SizedBox(
            height: 100,
          ),
          Icon(
            Icons.lightbulb_outline,
            size: 100,
            color: _bulbColor,
          ),
          Container(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Radio(
                    value: 'red',
                    groupValue: colorGroupValue,
                    onChanged: (val) {
                      colorGroupValue = val.toString();
                      if (_isActive) {
                        _bulbColor = Color.fromARGB(127, 243, 63, 50);
                      } else {
                        _bulbColor = Color.fromARGB(255, 243, 63, 50);
                      }

                      setState(() {});
                    }),
                Text('Rojo',
                    style: TextStyle(fontSize: 24, color: Colors.white))
              ],
            ),
          ),
          Container(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Radio(
                    value: 'blue',
                    groupValue: colorGroupValue,
                    onChanged: (val) {
                      colorGroupValue = val.toString();
                      if (_isActive) {
                        _bulbColor = Color.fromARGB(127, 33, 150, 243);
                      } else {
                        _bulbColor = Color.fromARGB(255, 33, 150, 243);
                      }

                      setState(() {});
                    }),
                Text('Azul',
                    style: TextStyle(fontSize: 24, color: Colors.white))
              ],
            ),
          ),
          Container(
            width: 150,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Radio(
                    value: 'green',
                    groupValue: colorGroupValue,
                    onChanged: (val) {
                      colorGroupValue = val.toString();
                      if (_isActive) {
                        _bulbColor = Color.fromARGB(127, 76, 175, 80);
                      } else {
                        _bulbColor = Color.fromARGB(255, 76, 175, 80);
                      }

                      setState(() {});
                    }),
                Text('Verde',
                    style: TextStyle(fontSize: 24, color: Colors.white))
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Checkbox(
                    value: _isActive,
                    onChanged: (_isActive) =>
                        setState(() => this._isActive = _isActive!)),
                SizedBox(
                  height: 150,
                ),
                Text('Transparencia   ',
                    style: TextStyle(fontSize: 24, color: Colors.white)),
                FlatButton(
                  color: Colors.blueGrey,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  onPressed: () {
                    if (_isActive &&
                        (_bulbColor == Color.fromARGB(255, 0, 0, 0) ||
                            _bulbColor == Color.fromARGB(127, 0, 0, 0))) {
                      _bulbColor = Color.fromARGB(127, 0, 0, 0);
                    } else if (!_isActive &&
                        (_bulbColor == Color.fromARGB(255, 0, 0, 0) ||
                            _bulbColor == Color.fromARGB(127, 0, 0, 0))) {
                      _bulbColor = Color.fromARGB(255, 0, 0, 0);
                    } else if (_isActive &&
                        (_bulbColor == Color.fromARGB(255, 243, 63, 50) ||
                            _bulbColor == Color.fromARGB(127, 243, 63, 50))) {
                      _bulbColor = Color.fromARGB(127, 243, 63, 50);
                    } else if (!_isActive &&
                        (_bulbColor == Color.fromARGB(255, 243, 63, 50) ||
                            _bulbColor == Color.fromARGB(127, 243, 63, 50))) {
                      _bulbColor = Color.fromARGB(255, 243, 63, 50);
                    } else if (_isActive &&
                        (_bulbColor == Color.fromARGB(255, 33, 150, 243) ||
                            _bulbColor == Color.fromARGB(127, 33, 150, 243))) {
                      _bulbColor = Color.fromARGB(127, 33, 150, 243);
                    } else if (!_isActive &&
                        (_bulbColor == Color.fromARGB(255, 33, 150, 243) ||
                            _bulbColor == Color.fromARGB(127, 33, 150, 243))) {
                      _bulbColor = Color.fromARGB(255, 33, 150, 243);
                    } else if (_isActive &&
                        (_bulbColor == Color.fromARGB(255, 76, 175, 80) ||
                            _bulbColor == Color.fromARGB(127, 76, 175, 80))) {
                      _bulbColor = Color.fromARGB(127, 76, 175, 80);
                    } else if (!_isActive &&
                        (_bulbColor == Color.fromARGB(255, 76, 175, 80) ||
                            _bulbColor == Color.fromARGB(127, 76, 175, 80))) {
                      _bulbColor = Color.fromARGB(255, 76, 175, 80);
                    }
                    setState(() {});
                  },
                  child: Text("Aplicar",
                      style: TextStyle(fontSize: 15, color: Colors.white)),
                )
              ],
            ),
          )
        ],
      )),
    ));
  }
}
