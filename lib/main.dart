import 'package:flutter/material.dart';
import 'package:unidad_1_y_2/pages/page01.dart';
import 'package:unidad_1_y_2/pages/page02.dart';
import 'package:unidad_1_y_2/pages/page03.dart';
import 'package:unidad_1_y_2/pages/page04.dart';
import 'package:unidad_1_y_2/pages/page05.dart';
import 'package:unidad_1_y_2/pages/page06.dart';
import 'package:unidad_1_y_2/pages/page07.dart';
import 'package:unidad_1_y_2/pages/page08.dart';
import 'package:unidad_1_y_2/pages/page09.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _lista = [
    '1.1 Layouts, botones, etiquetas y cuadros de texto',
    '1.2 Checkboxes y Radio Buttons',
    '1.3 Listas simples y desplegables',
    '1.4 Grids y Pestañas',
    '1.5 Action Bar y Menús',
    '1.6 Imágenes y elementos multimedia',
    '2.2 Geolocalización',
    '2.3 Orientación de los dispositivos',
    '2.4 Movimiento en los dispositivos'
  ];

  String _vista = 'Seleccione un tema';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/Background.jpg"), fit: BoxFit.cover),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Unidad 1 & 2",
                  style: TextStyle(
                      fontSize: 50.0,
                      color: Color.fromARGB(255, 224, 224, 224),
                      fontWeight: FontWeight.w400)),
              SizedBox(height: 50.0),
              DropdownButton(
                  items: _lista.map((String a) {
                    return DropdownMenuItem(value: a, child: Text(a));
                  }).toList(),
                  onChanged: (_value) => {
                        setState(() {
                          _vista = _value.toString();
                        }),
                        if (_vista ==
                            '1.1 Layouts, botones, etiquetas y cuadros de texto')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page01()))
                          }
                        else if (_vista == '1.2 Checkboxes y Radio Buttons')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page02()))
                          }
                        else if (_vista == '1.3 Listas simples y desplegables')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page03()))
                          }
                        else if (_vista == '1.4 Grids y Pestañas')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page04()))
                          }
                        else if (_vista == '1.5 Action Bar y Menús')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page05()))
                          }
                        else if (_vista ==
                            '1.6 Imágenes y elementos multimedia')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page06()))
                          }
                        else if (_vista == '2.2 Geolocalización')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page07()))
                          }
                        else if (_vista ==
                            '2.3 Orientación de los dispositivos')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page08()))
                          }
                        else if (_vista == '2.4 Movimiento en los dispositivos')
                          {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => page09()))
                          }
                      },
                  hint: Text(_vista,
                      style: TextStyle(
                          color: Color.fromARGB(255, 224, 224, 224)))),
              SizedBox(height: 50.0),
              SizedBox(
                  width: 250,
                  height: 300,
                  child: Image.asset('assets/itDelicias.png')),
              SizedBox(height: 50.0),
              Text("José Abelardo Castillo Flores",
                  style: TextStyle(color: Color.fromARGB(255, 224, 224, 224))),
              Text("17540037",
                  style: TextStyle(color: Color.fromARGB(255, 224, 224, 224)))
            ],
          ),
        ),
      ),
    );
  }
}
